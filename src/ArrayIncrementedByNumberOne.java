import java.util.Arrays;

public class ArrayIncrementedByNumberOne {
    public static void main(String[] args) {
        int [] array = {1, 9, 9, 9};
        System.out.println(Arrays.toString(addOne(array)));
    }

    public static int [] addOne (int givenArray []){
        int carry = 1;
        int sum;
        int [] result = new int [givenArray.length];

        for (int i = givenArray.length - 1; i >= 0; i--){
             sum = givenArray[i] + carry;
             if (sum == 10){
                 carry = 1;
             }
             else {
                 carry = 0;
             }
            result [i] = sum % 10;
        }
        if (carry == 1){
            result = new int [givenArray.length + 1];
            result [0] = 1;
        }
        return result;
    }
}
