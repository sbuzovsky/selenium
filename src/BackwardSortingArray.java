import java.util.Arrays;

public class BackwardSortingArray {

    public static void main(String[] args) {
        int [] array = {1,2,3,4,5};
        String string = "Hello world";
        char [] arrayOfChars = {'h','e','l','l','o'};

        System.out.println(sortedArrayOfIntegers(array));
        System.out.println(sortedString(string));
        sortedArrayOfChars(arrayOfChars);
    }
    public static String sortedArrayOfIntegers(int[] arr) {

        Arrays.sort(arr);
        String result = "";

        for (int i = arr.length-1; i>=0; i--){
            result = result + arr[i];
        }
        return result;
    }

    public static String sortedString(String str) {
        char [] charString = str.toCharArray();
        String resultString = "";
        for(int i = charString.length - 1; i>=0; i--){
            resultString = resultString + charString[i];
        }
        return resultString;
    }

    public static void sortedArrayOfChars(char [] array) {
        int j = array.length-1;
        char temp;

        for (int i = 0; i <= j; i++){
            temp = array[i];
            array[i] = array[j];
            array [j] = temp;
        }
        System.out.println(array);

    }
}