public class BaloonsNumber {
    public static void main(String[] args) {

        String sample = "balloon";
        String text = "loonbalxballpoon";

        System.out.println(maxNumberOfBalloons(text, sample));
    }

    public static int maxNumberOfBalloons(String text, String sample) {
        StringBuilder sb =  new StringBuilder(text);
        int ptr = 0, ans = 0;

        while(true)
        {
            if(ptr == sample.length())
            {
                ptr = 0;
                ans++;
            }

            String temp = Character.toString(sample.charAt(ptr));
            int index = sb.indexOf(temp);
            if(index >= 0) sb.delete(index,index+1);
            else break;
            ptr++;
        }
        return ans;
    }
}
