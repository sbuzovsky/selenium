public class BinarySearch {
    public static void main(String[] args) {
        int[] array = {2, 16, 24, 57, 74};
        int left = 0;
        int right = array.length-1;

        searchBinary(array, 2, left, right);
        binarySearchRecursion(array, 2, left, right);

    }

    public static int searchBinary(int[] array, int elementToFind, int startIndex, int endIndex) {
        int middleIndex;
        while (startIndex <= endIndex) {
            middleIndex = (startIndex + endIndex) / 2;

            if (array[middleIndex] == elementToFind) {
                System.out.println("found " + elementToFind + " at " + middleIndex);
                return middleIndex;
            }

            if (array[middleIndex] > elementToFind) {
                endIndex = middleIndex - 1;
            } else {
                startIndex = middleIndex + 1;
            }
        }
        return -1;
    }

    public static void binarySearchRecursion (int [] array, int elementToFind, int left, int right){
        if (left > right){
            System.out.println("No such element");
            return;
        }
        int middleIndex = (left + right) / 2;

        if (array[middleIndex] == elementToFind){
            System.out.println(middleIndex);
            return;
        } else if (array[middleIndex] > elementToFind){
            binarySearchRecursion(array, elementToFind, left, middleIndex - 1);
        } else if (array[middleIndex] < elementToFind){
            binarySearchRecursion(array, elementToFind, middleIndex + 1, right);
        }
    }
}
