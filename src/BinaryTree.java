public class BinaryTree {
    public static void main(String[] params) {
        Tree root =
                new Tree(20,
                        new Tree(7,
                                new Tree(4, null, new Tree(6)), new Tree(9)),
                        new Tree(35,
                                new Tree(31, new Tree(28), null),
                                new Tree(40, new Tree(38), new Tree(52))));

        System.out.println("Сумма дерева: " + root.sum());
    }

    static class Tree {
        int rootValue;
        Tree leftBranch;
        Tree rightBranch;

        public Tree(int rootValue, Tree leftBranch, Tree rightBranch) {
            this.rootValue = rootValue;
            this.leftBranch = leftBranch;
            this.rightBranch = rightBranch;
        }

        public Tree(int leafValue) {
            this.rootValue = leafValue;
        }

        public int sum() {
            int summ = rootValue;

            if (leftBranch != null) {
                summ += leftBranch.sum();
            }

            if (rightBranch != null) {
                summ += rightBranch.sum();
            }
            return summ;
        }
    }
}
