import java.util.Arrays;

public class BubbleSorting {
        public static void main(String[] args) {
            int [] array = {11, 3, -9, 14, 16, 7};

            boolean swapNeeded = true;
            while(swapNeeded) {
                swapNeeded = false;
                for (int i = 1; i < array.length; i++) {
                    if(array[i] < array[i-1]){

                        int temp = array[i];
                        array[i] = array[i-1];
                        array[i-1] = temp;
                        swapNeeded = true;
                    }
                }
            }
            System.out.println(Arrays.toString(array));
        }
    }
