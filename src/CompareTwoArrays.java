public class CompareTwoArrays {
    public static void main(String[] args) {
        int [] a1 = {12,12,56,8,6};
        int [] a2 = {12,12,56,8,6};

        System.out.println(compareArr(a1, a2));
    }
    static boolean compareArr (int [] arr1, int [] arr2){

        if (arr1.length != arr2.length) {
            return false;
        }
        for(int i = 0; i < arr1.length; i++){
            if(arr1[i] != arr2[i]){
                return false;
            }
        }
        return true;
    }
}
