//Factorial - 1*2*3*4... until the number we want.

public class Factorial {
    public static void main(String[] args) {
        System.out.println(fact(9));
    }
    static int fact(int x) {
        if (x >= 1) {
            return x * fact(x - 1);
        } else {
            return 1;
        }
    }
}
