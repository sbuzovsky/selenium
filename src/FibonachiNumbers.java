public class FibonachiNumbers {

    public static void main(String[] args){
        int n1 = 1;
        int n2 = 1;
        int n3;
        System.out.print(n1+" "+n2+" ");
        for(int i = 3; i <= 11; i++){
            n3=n1+n2;
            System.out.print(n3+" ");
            n1=n2;
            n2=n3;
        }
        System.out.println();

        System.out.println(fibNum(10));

        System.out.println(fibNumRecursion(6));

    }

    public static long fibNum (int n){
        long [] arr = new long [n + 1];
        arr [0] = 0;
        arr [1] = 1;
        for (int i = 2; i <= n; i++){
            arr [i] = arr [i-2] + arr [i-1];
        }
        return arr[n];
    }

    public static int fibNumRecursion (int number){
        if(number == 1 || number == 2){
            return 1;
        }
        return fibNumRecursion(number - 1) + fibNumRecursion(number - 2);
    }
}
