public class FizzBuzzRecursion {

    public static void main(String[] args) {
        rec (1, 100);

    }
    public static void rec (int a, int b){
        if (a <= b) {

            if (a % 15 == 0) {
                System.out.println("fizzbuzz" + " ");
            } else if (a % 5 == 0) {
                System.out.println("buzz" + " ");
            } else if (a % 3 == 0) {
                System.out.println("fizz" + " ");
            } else {
                System.out.println(a + " ");
            }
            rec(a+1, b);
        }

    }

}
