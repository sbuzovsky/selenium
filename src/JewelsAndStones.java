//How many jewels in stones? Characters in J are distinct.

public class JewelsAndStones {
    public static void main(String[] args) {
        String jewelsString = "aA"; //jewels
        String stonesString = "aAAbbbb"; //stones

        System.out.println(numberOfJewelsInStones(jewelsString, stonesString));

    }

    static int numberOfJewelsInStones(String jewels, String stones) {
        int result = 0;

        for(int j = 0; j < jewels.length(); j++){
            for(int s = 0; s < stones.length(); s++){
                if(jewels.charAt(j) == stones.charAt(s)){
                    result++;
                }
            }
        }
        return result;
    }

}
