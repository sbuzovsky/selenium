// Java program to find the last k digits in product of array

public class LastDigitsInProductOfArray {

    // Returns last k digits in product of arr[]
    static int lastKDigits(int arr[], int n, int k) {

        int numberOfHundreds = (int)(Math.pow(10, k));

        // Multiplying array elements under modulo 10^k.
        int x = arr[0] % numberOfHundreds;

        for (int i = 1; i < n; i++) {
            arr[i] = arr[i] % numberOfHundreds;
            x = (arr[i] * x) % numberOfHundreds;
        }
        return x;
    }

    // Driven program
    public static void main(String args[])
    {
        int arr[] = { 25, 33, 47, 9975};
        int k = 2;
        int n = arr.length;

        System.out.println(lastKDigits(arr, n, k));
    }
}
