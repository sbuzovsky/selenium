public class LinearSearch {
    public static void main(String[] args) {
        int [] array = {21, 3, 46, 90, 88, 712, 2, 3, 12, 171};

        linearSearch(array, 712);

    }

    static int linearSearch (int [] arr, int element){
        for(int i = 0; i < arr.length; i++){

            System.out.println("Searching at index" + i + ", value: " + arr[i]);

            if(arr[i] == element){

                System.out.println("Found at index: " + i + ", value: " + arr[i]);

                return i;
            }
        }
        System.out.println("Not found: " + element);

        return -1;
    }


}
