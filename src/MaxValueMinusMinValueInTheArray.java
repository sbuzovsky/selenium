public class MaxValueMinusMinValueInTheArray {
    public static void main(String[] args) {
        int [] arr = {1, 6, 7, 3, -5, 10, 2};
        System.out.println(substractMinFromMaxValue(arr));

    }
    public static int substractMinFromMaxValue (int [] arr){
        if (arr.length < 1){
            return -1;
        }
        int min = arr [0];
        int max = arr [0];
        for (int i = 0; i < arr.length; i++){
            if (arr[i] > max){
                max = arr[i];
            }
            if (arr[i] < min){
                min = arr[i];
            }
        }
        return max - min;
    }
}
