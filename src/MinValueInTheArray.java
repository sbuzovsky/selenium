public class MinValueInTheArray {

    public static void main(String[] args) {
        int [] arr = {56, 12, 34, 22, 12, 90, 99};

        int minValue = arr[0];
        int indexOfMinValue = 0;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue){
                minValue = arr[i];
                indexOfMinValue = i;
            }
        }

        System.out.println(minValue + "\n" + indexOfMinValue);

    }

}
