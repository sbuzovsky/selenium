import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MostRepeatedCharacterInString {

    public static void main(String[] args) {
        String str = "google";
        System.out.println(mostRepeatedChar(str));
    }
    public static char mostRepeatedChar (String str){
        if (str.isEmpty()){
            return '0';
        }
        HashMap <Character, Integer> result = new HashMap<>();
        for (int i = 0; i < str.length(); i++){
            Character character = str.charAt(i);

            if (!result.containsKey(character)){
                result.put(character, i);
            } else {
                int currentValue = result.get(character);
                currentValue++;
                result.put(character, currentValue);
            }
        }

        Iterator it = result.entrySet().iterator();
        int maxValue = 0;
        Character winner = str.charAt(0);
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            if ((Integer)pair.getValue() > maxValue){
                maxValue = (Integer)pair.getValue();
                winner = (Character)pair.getKey();
            }
        }
        return winner;
    }
}
