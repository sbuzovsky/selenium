import java.util.Arrays;

public class MoveZeroes {
    public static void main(String[] args) {
        int [] nums = {0,0,0,0,0,0,7,0,0,0,0,0,0,12};

        moveZeroes(nums);
    }
    public static void moveZeroes (int [] nums) {
        int writePointer = 0;
        for(int readPointer = 0; readPointer < nums.length; readPointer++){
            if(nums[readPointer] != 0){
                nums[writePointer] = nums[readPointer];
                writePointer++;
            }
        }
        for(int i = writePointer; i < nums.length; i++){
            nums[i] = 0;
        }
        System.out.println(Arrays.toString(nums));
    }
}
