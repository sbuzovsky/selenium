//1. What's the input parameter?
//2. What's the datatype of the output that the function has to return?
//3. What data structures and methods associated with it will be used?
//4. Where are you going to store the temporary data (if needed)?
//5. Describe the model of your algorithm before implementing solution in code
//6. Write unit tests for your solution -- think about edge cases
//7. Runtime complexity?
//8. Memory complexity?


public class Palindrome {
    public static void main(String[] args) {
        String word = "racecar";
        String sentence = "A man, a plan a canal: Panama";
        int number = 112211;

        System.out.println(wordIsPalindrome(word));
        sentenceIsPalindrome(sentence);
        System.out.println(numberIsPalindrome(number));

    }
    public static boolean wordIsPalindrome(String string){
        for(int i = 0; i < string.length() / 2; i++){
            int startChar = string.charAt(i);
            int endChar = string.charAt(string.length() - 1 - i);

            if(startChar != endChar){
                return false;
            }
        }
        return true;
    }

    public static void sentenceIsPalindrome(String sentence) {
        int start = 0;
        int end = sentence.length() - 1;
        while (start < end) {
            if (!Character.isLetterOrDigit(sentence.charAt(start))) {
                start++;
                continue;
            }
            if (!Character.isLetterOrDigit(sentence.charAt(end))) {
                end--;
                continue;
            }
            if (Character.toLowerCase(sentence.charAt(start)) != Character.toLowerCase(sentence.charAt(end))) {
                System.out.println("Sentence is not palindrome");
                break;
            }
            else {
                start++;
                end--;
            }
        }
        if (start == end){
            System.out.println("Sentence is palindrome");
        }
    }

    public static boolean numberIsPalindrome (int number){
        int oldNumber = number;
        int reverseNumber = 0;

        while (number > 0){
            int remainder = number % 10;
            reverseNumber = reverseNumber * 10 + remainder;
            number = number / 10;
        }
        return oldNumber == reverseNumber;
    }
}
