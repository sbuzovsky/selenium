public class PrimeNumber {
    public static void main (String[] args){
        int num = 97;
        boolean flag = true;

        for(int i=2; i * i < num; i++){
            //i * i means we don't have to
            // go through all numbers but just
            // till the half of them
            if (num % i == 0){
                flag = false;
                break;
            }
        }
        if (flag && num > 1){
            System.out.println("Number is Prime");
        }
        else {
            System.out.println("Number is not Prime");
        }
    }
}