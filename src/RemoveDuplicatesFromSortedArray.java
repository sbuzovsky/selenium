public class RemoveDuplicatesFromSortedArray {
    public static void main(String[] args) {
        int [] nums = {0,1,1,1,2,2,3,3,4};

        System.out.println(removeDuplicates(nums));

    }
    public static int removeDuplicates (int [] arr){
        if (arr.length == 0){
            return 0;
        }
        int i = 0;
        for (int j = 1; j < arr.length; j++){
            if(arr[j] != arr[i]){
                arr[i] = arr[j];
                i++;
            }
        }
        return i+1;
    }
}
