public class SecondMinimalValueInArray {
    public static void main(String[] args) {
        int [] arr = new int [] {64, 56, 42, 99, 6, 12, 67, 90, 32, 89};
        
        findSecondMinValue(arr);
    }

    static void findSecondMinValue(int [] array){
        int minValue = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        for (int i = 0; i < array.length; i++){
            if(minValue > array[i]){
                secondMin = minValue;
                minValue = array[i];
            }
            else if (array[i] < secondMin && array[i] != minValue){
                secondMin = array[i];
            }
        }
        if(secondMin != Integer.MAX_VALUE){
            System.out.println(secondMin);
        }
        else {
            System.out.println("No element");
        }
    }
}
