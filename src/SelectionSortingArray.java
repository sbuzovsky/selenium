//Algorithm consists of two steps: seeking the minimum value
// and changing it with the "starting" value of the array

import java.util.Arrays;

public class SelectionSortingArray {
    public static void main(String[] args) {

        int [] array = {56, 76, 34, 22, 12, 90, 99};

//number of sorting steps are always equal to the number of array elements
        for (int step = 0; step < array.length; step++){

            int index = chooseMinValue(array, step); //index of the minimum element of our array
            int temp = array[step];
            array[step] = array[index];
            array[index] = temp;
        }

        System.out.println(Arrays.toString(array));

    }

    static int chooseMinValue(int [] arr, int startingValue){

        int indexOfMinValue = startingValue;
        int minValue = arr[startingValue];

        for (int i = startingValue + 1; i < arr.length; i++) {
            if (arr[i] < minValue){
                minValue = arr[i];
                indexOfMinValue = i;
            }
        }
        return indexOfMinValue;
    }
}
