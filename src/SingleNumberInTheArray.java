public class SingleNumberInTheArray {
    public static void main(String[] args) {

        int [] nums = {4,1,2,1,2};
        System.out.println(solution(nums));

    }
    public static int solution (int [] nums){
        int result = 0;
        for(int i : nums) {
            result ^= i;
        }
        return result;
    }
}