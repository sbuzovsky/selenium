import java.util.HashSet;
import java.util.Set;

public class SmallestPositiveIntegerDoesntOccurInTheArray {
    public static void main(String[] args) {
        int [] A = {3, 3, 5, 6, 1, 2, 1};

        System.out.println(solution(A));
    }
    public static int solution(int[] A) {
        int N = A.length;

        Set<Integer> integers = new HashSet<>();
        for (int x : A) {
            if (x > 0) {
                integers.add(x);
            }
        }
//        if (integers.size() == 0) {
//            return N = 1;
//        }

        for (int i = 1; i <= N+1; i++) {
            if (!integers.contains(i)) {
                N = i;
                break; }
        }
        return N;
    }
}
